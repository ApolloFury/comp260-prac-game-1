﻿using UnityEngine;
using System.Collections;


public class BeeSpawner : MonoBehaviour {


	public int nBees = 50;
	public BeeMove beePrefab;
	//public PlayerMove player;
	public float xMin, yMin;
	public float width, height;


	//Random beePeriods
	public float minBeePeriod;
	public float maxBeePeriod;
	private float beeTimer;


	// Use this for initialization
	void Start () {

		randomBees ();
		InvokeRepeating ("spawnBees",0,beeTimer);
	}
	
	// Update is called once per frame
	void Update () {



	}


	public void randomBees(){
	
		beeTimer = Random.Range (minBeePeriod, maxBeePeriod);

	}
		
	public void spawnBees(){
	
		for (int i = 0; i < nBees; i++) {

			BeeMove bee = Instantiate (beePrefab);
			bee.transform.parent = transform; 
			bee.gameObject.name = "Bee " + i;
			//bee.target = player.transform;
			float x = xMin + Random.value * width;
			float y = yMin + Random.value * height;
			bee.transform.position = new Vector2 (x, y);
		}
	}

	public void DestroyBees(Vector2 centre, float radius) {
		// destroy all bees within ‘radius’ of ‘centre’
		for (int i = 0; i < transform.childCount; i++) {
			Transform child = transform.GetChild (i);
			// FIXED
			Vector2 v = (Vector2)child.position - centre;
			if (v.magnitude <= radius) {
				Destroy (child.gameObject);
			}
		}
	}


}
