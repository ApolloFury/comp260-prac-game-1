﻿using UnityEngine;
using System.Collections;

public class Player2Move : MonoBehaviour {
	private BeeSpawner beeSpawner;
	public float destroyRadius = 1.0f;

	public Vector2 move;
	public Vector2 velocity;
	public float maxSpeed = 5.0f;

	// Use this for initialization
	void Start () {
		beeSpawner = FindObjectOfType<BeeSpawner> ();
	}

	// Update is called once per frame
	void Update () {
		if (Input.GetButton ("Fire1")) {
			// destroy nearby bees
			beeSpawner.DestroyBees(
				transform.position, destroyRadius);
		}
			
		Vector2 direction;
		direction.x = Input.GetAxis ("Horizontal2");
		direction.y = Input.GetAxis ("Vertical2");

		Vector2 velocity = direction * maxSpeed;
		transform.Translate (velocity * Time.deltaTime);

	

	}
}
