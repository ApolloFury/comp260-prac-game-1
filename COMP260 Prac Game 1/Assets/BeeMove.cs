﻿using UnityEngine;
using System.Collections;

public class BeeMove : MonoBehaviour {

	public ParticleSystem explosionPrefab;

	public float minSpeed, maxSpeed;
	public float minTurnSpeed, maxTurnSpeed;

	private float speed;
	private float turnSpeed;

	//public float speed = 4.0f;
	//public float turnSpeed = 180.0f;
	public Transform target; //default is player 1
	private GameObject player1;
	private GameObject player2;
	public Vector2 heading = Vector3.right;

	private Vector3 player1Distance;
	private Vector3 player2Distance;

	// Use this for initialization
	void Start () {
		player1 = GameObject.Find ("Player");
		player2 = GameObject.Find ("Player2");
		//target = player1.transform;

		heading = Vector2.right;
		float angle = Random.value * 360;
		heading = heading.Rotate(angle);

		// set speed and turnSpeed randomly 
		speed = Mathf.Lerp(minSpeed, maxSpeed, Random.value);
		turnSpeed = Mathf.Lerp(minTurnSpeed, maxTurnSpeed, 
			Random.value);
		


	}
	
	// Update is called once per frame
	void Update () {
		player1Distance = player1.transform.position - transform.position;
		player2Distance = player2.transform.position - transform.position;


		if (player1Distance.magnitude < player2Distance.magnitude) {
			target = player1.transform;
		} else {
			target = player2.transform;
		}

		Vector2 direction = target.position - transform.position;


		float angle = turnSpeed * Time.deltaTime;

		if (direction.IsOnLeft (heading)) {
			heading = heading.Rotate (angle);
		} 
		else {
			heading = heading.Rotate (-angle);
		}

		transform.Translate (heading * speed * Time.deltaTime);
	}

	void OnDrawGizmos() {
		// draw heading vector in red
		Gizmos.color = Color.red;
		Gizmos.DrawRay(transform.position, heading);

		// draw target vector in yellow
		Gizmos.color = Color.yellow;
		Vector2 direction = target.position - transform.position;
		Gizmos.DrawRay(transform.position, direction);
	}

	void OnDestroy() {
		// create an explosion at the bee's current position
		ParticleSystem explosion = Instantiate(explosionPrefab);
		explosion.transform.position = transform.position;
		Destroy(explosion.gameObject, explosion.duration);
	}



}
